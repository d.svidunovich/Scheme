#include <iostream>

#include "lispp/Interpreter.h"

int main() {
    Interpreter interpreter;
    interpreter.Interprete(&std::cin, &std::cout);
    return 0;
}
