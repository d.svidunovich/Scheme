#pragma once

#include "../lispp/Interpreter.h"

#include <string>

#include <gtest/gtest.h>

struct LispTest : public ::testing::Test {
    // initialize new interpreter here
    Interpreter interpreter;
    LispTest() {}

    // evaluate expression and compare result to expected
    void ExpectEq(std::string expression, std::string result) {
        std::string ires = interpreter.Interprete(expression);
        if (ires.empty()) {
            FAIL() << "Expression: '" << expression << "'" << std::endl <<
                "Got empty" << std::endl;
        }
        if (ires != result) {
            FAIL() << "Expression: '" << expression << "'" << std::endl <<
                "Expected: '" << result << "'" << std::endl <<
                "Got: '" << ires << "'" << std::endl;
        }
    }

    // evaluate expression and check that there is not errors
    void ExpectNoError(std::string expression) {
        std::string ires = interpreter.Interprete(expression);
        if (ires.empty()) {
            FAIL() << "Expression: '" << expression << "'" << std::endl <<
                "Got empty" << std::endl;
        }

        if (ires == "syntax error" || ires == "runtime error" ||
				ires == "name error") {
            FAIL() << "Expression: '" << expression << "'" << std::endl <<
                "No exception Expected" << std::endl <<
                "Got: '" << ires << "'" << std::endl;
        }
    }

    // check that expression evaluation produces syntax error
    void ExpectSyntaxError(std::string expression) {
        std::string ires = interpreter.Interprete(expression);
        if (ires.empty()) {
            FAIL() << "Expression: '" << expression << "'" << std::endl <<
                "Got empty" << std::endl;
        }

        if (ires != "syntax error") {
            FAIL() << "Expression: '" << expression << "'" << std::endl <<
                "Syntax exception Expected" << std::endl <<
                "Got: '" << ires << "'" << std::endl;
        }
    }

    // check that expression evaluation produces runtime error
    void ExpectRuntimeError(std::string expression) {
        std::string ires = interpreter.Interprete(expression);
        if (ires.empty()) {
            FAIL() << "Expression: '" << expression << "'" << std::endl <<
                "Got empty" << std::endl;
        }

        if (ires != "runtime error") {
            FAIL() << "Expression: '" << expression << "'" << std::endl <<
                "Runtime exception Expected" << std::endl <<
                "Got: '" << ires << "'" << std::endl;
        }
    }

    // check that expression evaluation produces name error
    void ExpectUndefinedVariable(std::string expression) {
        std::string ires = interpreter.Interprete(expression);
        if (ires.empty()) {
            FAIL() << "Expression: '" << expression << "'" << std::endl <<
                "Got empty" << std::endl;
        }

        if (ires != "name error") {
            FAIL() << "Expression: '" << expression << "'" << std::endl <<
                "Name exception Expected" << std::endl <<
                "Got: '" << ires << "'" << std::endl;
        }
    }
};
