#include "IntegerBuiltInFunctions.h"


std::shared_ptr<Cell>
        isNumberProc(const std::vector<std::shared_ptr<Cell> >& args) {

    if (std::all_of(args.begin(), args.end(),
                [](const std::shared_ptr<Cell> current) {
                    return current -> GetType() == Cell::NUMBER; 
                })) { 
        return std::shared_ptr<Cell> (new Cell("#t"));
    }

    return std::shared_ptr<Cell> (new Cell("#f"));
}


bool twoEqual(const std::shared_ptr<Cell> first,
        const std::shared_ptr<Cell> second) {
    if (first -> GetType() != second -> GetType()) {
        throw RuntimeException();
    }

    switch (first -> GetType()) {
        case Cell::SYMBOL: {
            return first -> GetSymbol() == second -> GetSymbol();
        }
        case Cell::NUMBER: {
            return first -> GetNumber() == second -> GetNumber();
        }
        case Cell::BUILT_IN_PROC: {
            return first.get() == second.get();
        }
        default: {
            auto it1 = first -> begin();
            auto it2 = second -> begin();
            for ( ; it1 != first -> end() && it2 != second -> end();
                    it1++, it2++) {
                if (!twoEqual(*it1, *it2)) {
                    return false;
                }
            }

            if (it1 != first -> end() || it2 != second -> end()) {
                return false;
            }
            return true;
        }
    }
}


bool twoLess(const std::shared_ptr<Cell> first,
        const std::shared_ptr<Cell> second) {
    if (first -> GetType() != second -> GetType()) {
        throw RuntimeException();
    }

    switch (first -> GetType()) {
        case Cell::SYMBOL: {
            return first -> GetSymbol() < second -> GetSymbol();
        }
        case Cell::NUMBER: {
            return first -> GetNumber() < second -> GetNumber();
        }
        case Cell::BUILT_IN_PROC: {
            /* TODO need exception? */
            return first.get() == second.get();
        }
        default: {
            auto it1 = first -> begin();
            auto it2 = second -> begin();
            for ( ; it1 != first -> end() && it2 != second -> end();
                    it1++, it2++) {
                if (!twoLess(*it1, *it2)) {
                    return false;
                }
            }

            if (it1 != first -> end() || it2 != second -> end()) {
                return it1 == first -> end();
            }
            return true;
        }
    }
}


typedef std::vector<bool (*)(const std::shared_ptr<Cell> first,
                const std::shared_ptr<Cell> second)> VCmp;

std::shared_ptr<Cell> ArgsCmp(const std::vector<std::shared_ptr<Cell> >& args,
        VCmp cmps, bool invert, bool And) {
    if (args.size() <= 1) {
        return std::shared_ptr<Cell> (new Cell("#t"));
    }

    auto previous = args.front();
    for (auto it = args.begin() + 1; it != args.end(); it++) {
        auto ptr = *it;
        bool result = And ? true : false;
        for (auto cit = cmps.begin(); cit != cmps.end(); cit++) {
            bool func = (**cit)(previous, ptr);
            if (And) {
                result &= invert ? !func : func;
            } else {
                result |= invert ? !func : func;
            }
        }

        if (!result) {
            return std::shared_ptr<Cell> (new Cell("#f"));
        }
        previous = ptr;
    }

    return std::shared_ptr<Cell> (new Cell("#t"));
}


std::shared_ptr<Cell>
        EqualProc(const std::vector<std::shared_ptr<Cell> >& args) {
    return ArgsCmp(args, {&twoEqual}, false, false);
}


std::shared_ptr<Cell>
        LessProc(const std::vector<std::shared_ptr<Cell> >& args) {
    return ArgsCmp(args, {&twoLess}, false, false);
}


std::shared_ptr<Cell>
        GreaterProc(const std::vector<std::shared_ptr<Cell> >& args) {
    return ArgsCmp(args, {&twoLess, &twoEqual}, true, true);
}


std::shared_ptr<Cell>
        LessEqualProc(const std::vector<std::shared_ptr<Cell> >& args) {
    return ArgsCmp(args, {&twoLess, &twoEqual}, false, false);
}


std::shared_ptr<Cell>
        GreaterEqualProc(const std::vector<std::shared_ptr<Cell> >& args) {
    return ArgsCmp(args, {&twoLess}, true, false);
}


void IntegerTypeCheck(const std::vector<std::shared_ptr<Cell> >& args) {
    if (!std::all_of(args.begin(), args.end(),
                [](const std::shared_ptr<Cell>& cell) {
                    return cell -> GetType() == Cell::NUMBER;  
                })) {
        throw RuntimeException();
    }
}


std::shared_ptr<Cell>
        PlusProc(const std::vector<std::shared_ptr<Cell> >& args) {
    IntegerTypeCheck(args);            
    int64_t result = 0;

    for (const std::shared_ptr<Cell>& cell: args) {
        result += cell -> GetNumber();
    }

    return std::make_shared<Cell> (result);
}


std::shared_ptr<Cell>
        MinusProc(const std::vector<std::shared_ptr<Cell> >& args) {
    IntegerTypeCheck(args);
    if (args.empty()) {
        throw RuntimeException();
    }

    int64_t result = (args.begin()) -> get() -> GetNumber();
    for (auto it = args.begin() + 1; it != args.end(); it++) {
        result -= it -> get() -> GetNumber();
    }

    return std::make_shared<Cell> (result);
}


std::shared_ptr<Cell>
        MulProc(const std::vector<std::shared_ptr<Cell> >& args) {
    IntegerTypeCheck(args); 
    int64_t result = 1;

    for (const std::shared_ptr<Cell>& cell: args) {
        result *= cell -> GetNumber();
    }

    return std::make_shared<Cell> (result);
}


std::shared_ptr<Cell>
        DivProc(const std::vector<std::shared_ptr<Cell> >& args) {
    IntegerTypeCheck(args);
    if (args.empty()) {
        throw RuntimeException();
    }

    int64_t result = (args.begin()) -> get() -> GetNumber();
    for (auto it = args.begin() + 1; it != args.end(); it++) {
        result /= it -> get() -> GetNumber();
    }

    return std::make_shared<Cell> (result);
}


std::shared_ptr<Cell>
        MinProc(const std::vector<std::shared_ptr<Cell> >& args) {
    IntegerTypeCheck(args);
    if (args.empty()) {
        throw RuntimeException();
    }

    int64_t result = args.begin() -> get() -> GetNumber();
    std::for_each(args.begin() + 1, args.end(), 
            [&](const std::shared_ptr<Cell>& cell) {
                result = std::min(result, cell -> GetNumber());
            });
    return std::make_shared<Cell> (result);
}


std::shared_ptr<Cell>
        MaxProc(const std::vector<std::shared_ptr<Cell> >& args) {
    IntegerTypeCheck(args);
    if (args.empty()) {
        throw RuntimeException();
    }

    int64_t result = args.begin() -> get() -> GetNumber();
    std::for_each(args.begin() + 1, args.end(), 
            [&](const std::shared_ptr<Cell>& cell) {
                result = std::max(result, cell -> GetNumber());
            });
    return std::make_shared<Cell> (result);
}

std::shared_ptr<Cell>
        AbsProc(const std::vector<std::shared_ptr<Cell> >& args) {
    if (args.size() != 1) {
        throw RuntimeException();
    }
    IntegerTypeCheck(args);

    return std::make_shared<Cell> (
            std::abs(args.begin() -> get() -> GetNumber()));
}
