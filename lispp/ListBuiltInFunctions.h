#pragma once

#include "Cell.h"
#include "Exceptions.h"

#include <memory>
#include <vector>
#include <algorithm>



std::shared_ptr<Cell>
        QuoteProc(std::shared_ptr<Cell> cell);

std::shared_ptr<Cell>
        isPairProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        isNullProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        isListProc(const std::vector<std::shared_ptr<Cell> >& args);

std::shared_ptr<Cell>
        ConsProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        CarProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        CdrProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        SetCarProc(std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        SetCdrProc(std::vector<std::shared_ptr<Cell> >& args);


std::shared_ptr<Cell>
        ListProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        ListRefProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        ListTailProc(const std::vector<std::shared_ptr<Cell> >& args);
