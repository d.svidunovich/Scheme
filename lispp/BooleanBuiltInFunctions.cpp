#include "BooleanBuiltInFunctions.h"


std::shared_ptr<Cell>
        isBooleanProc(const std::vector<std::shared_ptr<Cell> >& args) {
    if (args.size() != 1) {
        throw RuntimeException();
    }

    auto value = args.front();
    if (value -> GetType() == Cell::SYMBOL &&
            (value -> GetSymbol() == "#t" || value -> GetSymbol() == "#f")) {
        return std::make_shared<Cell> ("#t");
    }


    return std::make_shared<Cell> ("#f");
}


std::shared_ptr<Cell>
        NotProc(const std::vector<std::shared_ptr<Cell> >& args) {
    if (args.size() != 1) {
        throw RuntimeException();
    }

    auto value = args.front();
    if (value -> GetType() == Cell::SYMBOL &&
            value -> GetSymbol() == "#f") {
        return std::make_shared<Cell> ("#t");
    }

    return std::make_shared<Cell> ("#f");
}


std::shared_ptr<Cell>
        AndProc(const std::vector<std::shared_ptr<Cell> >& args) {
    std::shared_ptr<Cell> value = std::make_shared<Cell> ("#t");
    for (auto it = args.begin(); it != args.end(); it++) {
        value = Interpreter::Evaluate(*it, it -> get() -> GetEnvironment());
        if (value -> GetType() == Cell::SYMBOL &&
                value -> GetSymbol() == "#f") {
            return value;
        }
    }

    return value;
}


std::shared_ptr<Cell>
        OrProc(const std::vector<std::shared_ptr<Cell> >& args) {
    std::shared_ptr<Cell> value = std::make_shared<Cell> ("#f");
    for (auto it = args.begin(); it != args.end(); it++) {
        value = Interpreter::Evaluate(*it, it -> get() -> GetEnvironment());
        if (value -> GetType() == Cell::SYMBOL &&
                value -> GetSymbol() == "#t") {
            return value;
        }
    }

    return value;
}
