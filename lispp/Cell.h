#pragma once

#include "Tokenizer.h"
#include "Environment.h"
#include "Exceptions.h"

#include <memory>
#include <string>
#include <vector>


class Environment;

class Cell {
    public:
        enum CellType {
            SYMBOL,
            NUMBER,
            LIST,
            BUILT_IN_PROC,
            LAMBDA,
        };

        typedef std::shared_ptr<Cell>
            (*BuiltInProc)(const std::vector<std::shared_ptr<Cell> > &);

        Cell(CellType type = LIST);
        Cell(const std::string& value);
        Cell(int64_t value);
        Cell(BuiltInProc bip);
        Cell(const Cell& other) = default;
        Cell(Cell&& other) = default;
        ~Cell() = default;

        void Add(const std::shared_ptr<Cell>& cell);
        size_t Size() const;

        std::vector<std::shared_ptr<Cell> >::const_iterator begin() const;
        std::vector<std::shared_ptr<Cell> >::const_iterator end() const;

        std::vector<std::shared_ptr<Cell> >::iterator begin();
        std::vector<std::shared_ptr<Cell> >::iterator end();

        void SetType(CellType type);
        void SetEnvironment(std::shared_ptr<Environment> env);

        CellType GetType() const;
        std::string GetSymbol() const;
        int64_t GetNumber() const;
        BuiltInProc GetBuildInProc() const;
        std::shared_ptr<Environment> GetEnvironment() const;

        Cell& operator = (const Cell& other) = default;
        Cell& operator = (Cell&& other) = default;

        static std::shared_ptr<Cell> PrepareCell(Tokenizer *tokenizer);
        std::string ToString() const;

    private:
        std::string ListToString(int depth) const;

        CellType type_;
        std::string symbol_;
        int64_t number_;
        BuiltInProc bip_;
        std::shared_ptr<Environment> currentEnv_;
        std::vector<std::shared_ptr<Cell> > list_;
};
