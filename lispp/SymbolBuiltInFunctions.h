#pragma once

#include "Cell.h"
#include "Exceptions.h"

#include <memory>


std::shared_ptr<Cell>
        isSymbolProc(const std::vector<std::shared_ptr<Cell> >& args);
