#include "Environment.h"


Environment::Environment(std::shared_ptr<Environment> outerEnv):
    outerEnv_(outerEnv) {}


std::shared_ptr<Environment> Environment::find(
        const std::string& variable, std::shared_ptr<Environment> currentEnv) {
    if (currentEnv -> innerEnv_.find(variable) != 
            currentEnv -> innerEnv_.end()) {
        return std::shared_ptr<Environment> (currentEnv);
    }

    if (currentEnv -> outerEnv_ != nullptr) {
        return currentEnv -> outerEnv_ ->
            find(variable, currentEnv -> outerEnv_);
    }

    throw NameException();
}


void Environment::set(const std::string& variable,
        std::shared_ptr<Cell> value) {
    innerEnv_[variable] = value;
}


std::shared_ptr<Cell> Environment::operator[] (
        const std::string& variable) {
    auto it = innerEnv_.find(variable);
    if (it == innerEnv_.end()) {
        throw NameException();
    }

    return innerEnv_[variable];
}
