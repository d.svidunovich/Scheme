#pragma once

#include <exception>
#include <string>

class SyntaxException : public std::exception {
    public:
        virtual const char* what() const throw();
};


class NameException : public std::exception {
    public:
        virtual const char* what() const throw();
};


class RuntimeException : public std::exception {
    public:
        virtual const char* what() const throw();
};
