#include "Interpreter.h"


Interpreter::Interpreter() {
    globalEnvironment = std::make_shared<Environment>();
    InitializeGlobalEnvironment();
}


void Interpreter::Interprete(std::istream* in, std::ostream* out) {
    Tokenizer tokenizer(in);

    while (tokenizer.GetType() != Tokenizer::END) {
        try {
            std::shared_ptr<Cell> prepared = Cell::PrepareCell(&tokenizer);
            if (prepared) {
                auto result = Evaluate(prepared, globalEnvironment);
                (*out) << result -> ToString();
                if ((result -> ToString()).empty()) {
                    (*out) << std::endl;
                }
            } else {
                break;
            }
        } catch (const SyntaxException& e) {
            (*out) << e.what();
            break;
        } catch (const std::exception& e) {
            (*out) << e.what();
        }
    }
}


std::string Interpreter::Interprete(std::string input) {
    std::istringstream iss(input);
    std::ostringstream oss;

    Interprete(&iss, &oss);
    return oss.str();
}


std::shared_ptr<Cell> Interpreter::Evaluate(std::shared_ptr<Cell> cell,
        std::shared_ptr<Environment> currentEnv) {
    if (cell -> GetType() == Cell::SYMBOL) {
        if (cell -> GetSymbol() != "(" && cell -> GetSymbol() != ")") {
            return EvaluateSymbol(cell, currentEnv);
        } else {
            throw SyntaxException();
        }
    } else if (cell -> GetType() == Cell::NUMBER) {
        return cell;
    } else {
        return EvaluateList(cell, currentEnv);
    }
}


void Interpreter::InitializeGlobalEnvironment() {
    globalEnvironment -> set("#t",
            std::make_shared<Cell> ("#t"));
    globalEnvironment -> set("#f",
            std::make_shared<Cell> ("#f"));


    globalEnvironment -> set("number?",
            std::make_shared<Cell> (&isNumberProc));
    globalEnvironment -> set("=",
            std::make_shared<Cell> (&EqualProc));
    globalEnvironment -> set("<",
            std::make_shared<Cell> (&LessProc));
    globalEnvironment -> set(">",
            std::make_shared<Cell> (&GreaterProc));
    globalEnvironment -> set("<=",
            std::make_shared<Cell> (&LessEqualProc));
    globalEnvironment -> set(">=",
            std::make_shared<Cell> (&GreaterEqualProc));

    globalEnvironment -> set("+",
            std::make_shared<Cell> (&PlusProc));
    globalEnvironment -> set("-",
            std::make_shared<Cell> (&MinusProc));
    globalEnvironment -> set("*",
            std::make_shared<Cell> (&MulProc));
    globalEnvironment -> set("/",
            std::make_shared<Cell> (&DivProc));

    globalEnvironment -> set("min",
            std::make_shared<Cell> (&MinProc));
    globalEnvironment -> set("max",
            std::make_shared<Cell> (&MaxProc));
    globalEnvironment -> set("abs",
            std::make_shared<Cell> (&AbsProc));


    globalEnvironment -> set("boolean?",
            std::make_shared<Cell> (&isBooleanProc));
    globalEnvironment -> set("not",
            std::make_shared<Cell> (&NotProc));
    globalEnvironment -> set("and",
            std::make_shared<Cell> (&AndProc));
    globalEnvironment -> set("or",
            std::make_shared<Cell> (&OrProc));


    globalEnvironment -> set("symbol?",
            std::make_shared<Cell> (&isSymbolProc));

    globalEnvironment -> set("pair?",
            std::make_shared<Cell> (&isPairProc));
    globalEnvironment -> set("null?",
            std::make_shared<Cell> (&isNullProc));
    globalEnvironment -> set("list?",
            std::make_shared<Cell> (&isListProc));

    globalEnvironment -> set("cons",
            std::make_shared<Cell> (&ConsProc));
    globalEnvironment -> set("car",
            std::make_shared<Cell> (&CarProc));
    globalEnvironment -> set("cdr",
            std::make_shared<Cell> (&CdrProc));


    globalEnvironment -> set("list",
            std::make_shared<Cell> (&ListProc));
    globalEnvironment -> set("list-ref",
            std::make_shared<Cell> (&ListRefProc));
    globalEnvironment -> set("list-tail",
            std::make_shared<Cell> (&ListTailProc));
}


std::shared_ptr<Cell> Interpreter::EvaluateSymbol(std::shared_ptr<Cell> cell,
        std::shared_ptr<Environment> currentEnv) {
    auto environment = Environment::find(cell -> GetSymbol(), currentEnv);
    return (*environment)[cell -> GetSymbol()];
}


std::shared_ptr<Cell> Interpreter::EvaluateList(std::shared_ptr<Cell> cell,
        std::shared_ptr<Environment> currentEnv) {
    if (cell -> Size() == 0 ) {
        throw RuntimeException();
    }

    const std::string& keyword = cell -> begin() -> get() -> GetSymbol();
    if (keyword == "quote") {
        return QuoteProc(*(cell -> begin() + 1));
    } else if (keyword == "define") {
        return EvaluateDefine(cell, currentEnv);
    } else if (keyword == "set!") {
        return EvaluateSet(cell, currentEnv);
    } else if (keyword == "if") {
        return EvaluateIf(cell, currentEnv);
    } else if (keyword == "lambda") {
        return EvaluateLambda(cell, currentEnv);
    } else if (keyword == "set-car!" || keyword == "set-cdr!") {
        return EvaluateSetC(cell, currentEnv);
    }

    return EvaluateFuncs(cell, currentEnv);
}


std::shared_ptr<Cell> Interpreter::EvaluateIf(std::shared_ptr<Cell> cell,
        std::shared_ptr<Environment> currentEnv) {
    if (cell -> Size() < 3 || 4 < cell -> Size()) {
        throw SyntaxException();
    }

    auto it = cell -> begin() + 1;
    auto condition = Evaluate(*it++, currentEnv);
    if (!(condition -> GetType() == Cell::SYMBOL &&
                condition -> GetSymbol() == "#f")) {
        return Evaluate(*it, currentEnv);
    } else if (++it != cell -> end()) {
        return Evaluate(*it, currentEnv);
    }

    return std::make_shared<Cell> ();
}


std::shared_ptr<Cell> Interpreter::EvaluateDefine(std::shared_ptr<Cell> cell,
        std::shared_ptr<Environment> currentEnv) {
    if (cell -> Size() != 3) {
        throw SyntaxException();
    }

    auto it = cell -> begin() + 1;
    if (it -> get() -> GetType() == Cell::LIST) {
        std::string variable = it -> get() -> begin() -> get() -> GetSymbol();
        auto lambdaCell = std::make_shared<Cell> ();
        lambdaCell -> Add(std::make_shared<Cell> ("lambda"));
        lambdaCell -> Add(std::make_shared<Cell> ());

        auto params = *(lambdaCell -> begin() + 1);
        for (auto argit = it -> get() -> begin() + 1;
                argit != it -> get() -> end(); argit++) {
            params -> Add(*argit);
        }

        lambdaCell -> Add(*(++it));
        currentEnv -> set(variable,
                Evaluate(lambdaCell, currentEnv));
    } else {
        currentEnv -> set(it++ -> get() -> GetSymbol(),
                Evaluate(*it, currentEnv));
    }

    return std::make_shared<Cell> ("");
}


std::shared_ptr<Cell> Interpreter::EvaluateSet(std::shared_ptr<Cell> cell,
        std::shared_ptr<Environment> currentEnv) {
    auto it = cell -> begin() + 1;
    if (cell -> Size() != 3 || it -> get() -> GetType() != Cell::SYMBOL) {
        throw SyntaxException();
    }

    auto environment = Environment::find(
            it -> get() -> GetSymbol(), currentEnv);
    if (environment) {
        environment -> set(it++ -> get() -> GetSymbol(),
                Evaluate(*it, currentEnv));
        return std::make_shared<Cell> ("");
    }

    throw RuntimeException();
}


std::shared_ptr<Cell> Interpreter::EvaluateLambda(std::shared_ptr<Cell> cell,
        std::shared_ptr<Environment> currentEnv) {
    if (cell -> Size() < 3) {
        throw SyntaxException();
    }

    cell -> SetType(Cell::LAMBDA);
    auto lambdaEnv = std::make_shared<Environment> (currentEnv);
    cell -> SetEnvironment(lambdaEnv);
    return cell;
}


std::shared_ptr<Cell> Interpreter::EvaluateFuncs(std::shared_ptr<Cell> cell,
        std::shared_ptr<Environment> currentEnv) {
    if (cell -> Size() >= 1) {
        for (auto it = cell -> begin(); it != cell -> end(); it++) {
            if (it -> get() -> GetType() == Cell::SYMBOL &&
                    it -> get() -> GetSymbol() == ".") {
                throw SyntaxException();
            }
        }
    }
    const std::string& keyword = cell -> begin() -> get() -> GetSymbol();
    std::shared_ptr<Cell> function = Evaluate(*(cell -> begin()), currentEnv);

    std::vector<std::shared_ptr<Cell> > args;
    bool optimised = keyword == "or" || keyword == "and";

    for (auto it = cell -> begin() + 1; it != cell -> end(); it++) {
        args.push_back(optimised ? *it : Evaluate(*it, currentEnv));
        if (optimised) {
            args.back() -> SetEnvironment(currentEnv);
        }
    }

    if (function -> GetType() == Cell::BUILT_IN_PROC) {
        return (*function -> GetBuildInProc())(args);
    } else if (function -> GetType() == Cell::LAMBDA) {
        auto params = *(function -> begin() + 1);
        if (params -> Size() != args.size()) {
            throw SyntaxException();
        }

        auto lambdaEnv = function -> GetEnvironment();

        auto ait = args.begin();
        for (auto lit = params -> begin();
                lit != params -> end() && ait != args.end();
                lit++, ait++) {
            lambdaEnv -> set(lit -> get() -> GetSymbol(), *ait);
        }

        auto result = Evaluate(*(function -> begin() + 2), lambdaEnv);
        for (auto it = function -> begin() + 3; it != function -> end(); it++) {
            result = Evaluate(*it, lambdaEnv);
        }
        return result;
    }

    throw RuntimeException();
}


std::shared_ptr<Cell> Interpreter::EvaluateSetC(std::shared_ptr<Cell> cell,
        std::shared_ptr<Environment> currentEnv) {
    const std::string& keyword = cell -> begin() -> get() -> GetSymbol();
    std::vector<std::shared_ptr<Cell> > args;
    bool optimised = keyword == "or" || keyword == "and";

    for (auto it = cell -> begin() + 1; it != cell -> end(); it++) {
        args.push_back(optimised ? *it : Evaluate(*it, currentEnv));
        if (optimised) {
            args.back() -> SetEnvironment(currentEnv);
        }
    }

    if (keyword == "set-car!") {
        return SetCarProc(args);
    }

    return SetCdrProc(args);
}
