#include "ListBuiltInFunctions.h"


std::shared_ptr<Cell>
        QuoteProc(std::shared_ptr<Cell> cell) {
    return cell;
}


std::shared_ptr<Cell>
        isPairProc(const std::vector<std::shared_ptr<Cell> >& args) {
    return args.size() == 1 && args[0] -> GetType() == Cell::LIST &&
            args[0] -> Size() >= 2 ?
            std::make_shared<Cell> ("#t") : std::make_shared<Cell> ("#f");
}


std::shared_ptr<Cell>
        isNullProc(const std::vector<std::shared_ptr<Cell> >& args) {
    return args.size() == 1 && args[0] -> GetType() == Cell::LIST &&
            args[0] -> Size() == 0 ?
            std::make_shared<Cell> ("#t") : std::make_shared<Cell> ("#f");
}


std::shared_ptr<Cell>
        isListProc(const std::vector<std::shared_ptr<Cell> >& args) {
    bool result = std::all_of(args[0] -> begin(), args[0] -> end(), 
            [](const std::shared_ptr<Cell> cell) {
                return !(cell -> GetType() == Cell::SYMBOL &&
                        cell -> GetSymbol() == ".");
            });

    return std::make_shared<Cell> (result ? "#t" : "#f");
}


std::shared_ptr<Cell>
        ConsProc(const std::vector<std::shared_ptr<Cell> >& args) {
    if (args.size() != 2) {
        throw RuntimeException();
    }

    auto result = std::make_shared<Cell> ();
    result -> Add(args[0]);
    result -> Add(std::make_shared<Cell> ("."));
    result -> Add(args[1]);

    return result;
}


std::shared_ptr<Cell>
        CarProc(const std::vector<std::shared_ptr<Cell> >& args) {
    return *(args[0] -> begin());
}


std::shared_ptr<Cell>
        CdrProc(const std::vector<std::shared_ptr<Cell> >& args) {
    return *(args[0] -> begin() + 2);
}


std::shared_ptr<Cell>
        SetCarProc(std::vector<std::shared_ptr<Cell> >& args) {
    *(args.begin() -> get() -> begin()) = args[1]; 
    return std::make_shared<Cell> ("");
}


std::shared_ptr<Cell>
        SetCdrProc(std::vector<std::shared_ptr<Cell> >& args) {
    *(args.begin() -> get() -> begin() + 2) = args[1]; 
    return std::make_shared<Cell> ("");
}


std::shared_ptr<Cell>
        ListProc(const std::vector<std::shared_ptr<Cell> >& args) {
   auto result = std::make_shared<Cell> ();
   for (auto ptr: args) {
        result -> Add(ptr);
   }

   return result;
}


std::shared_ptr<Cell>
        ListRefProc(const std::vector<std::shared_ptr<Cell> >& args) {
    if (args.size() != 2 || args[0] -> GetType() != Cell::LIST ||
            args[1] -> GetType() != Cell::NUMBER) {
        throw SyntaxException();
    }

    int index = args[1] -> GetNumber();
    for (auto it = args[0] -> begin(); it != args[0] -> end(); it++) {
        if (index-- == 0) {
            return *it;
        }
    }

    throw RuntimeException();
}


std::shared_ptr<Cell>
        ListTailProc(const std::vector<std::shared_ptr<Cell> >& args) {
    if (args.size() != 2 || args[0] -> GetType() != Cell::LIST ||
            args[1] -> GetType() != Cell::NUMBER) {
        throw SyntaxException();
    }

    int index = args[1] -> GetNumber();
    auto result = std::make_shared<Cell> ();
    for (auto it = args[0] -> begin(); it != args[0] -> end(); it++) {
        if (index-- <= 0) {
            result -> Add(*it);
        }
    }

    if (index > 0) {
        throw RuntimeException();
    }

    return result;
}
