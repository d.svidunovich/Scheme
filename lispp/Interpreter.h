#pragma once

#include "Environment.h"
#include "Tokenizer.h"
#include "Exceptions.h"
#include "IntegerBuiltInFunctions.h"
#include "BooleanBuiltInFunctions.h"
#include "SymbolBuiltInFunctions.h"
#include "ListBuiltInFunctions.h"

#include <memory>
#include <iostream>
#include <sstream>
#include <exception>


class Interpreter {
    public:
        Interpreter();
        Interpreter(const Interpreter& other) = delete;
        Interpreter(Interpreter&& other) = default;
        ~Interpreter() = default;

        void Interprete(std::istream* in_, std::ostream* out_);
        std::string Interprete(std::string input);


        Interpreter& operator = (const Interpreter& other) = delete;
        Interpreter& operator = (Interpreter&& other) = default;

        static std::shared_ptr<Cell> Evaluate(std::shared_ptr<Cell> cell,
                std::shared_ptr<Environment> currentEnv);

    private:
        void InitializeGlobalEnvironment();

        static std::shared_ptr<Cell> EvaluateSymbol(std::shared_ptr<Cell> cell,
                std::shared_ptr<Environment> currentEnv);
        static std::shared_ptr<Cell> EvaluateList(std::shared_ptr<Cell> cell,
                std::shared_ptr<Environment> currentEnv);
        static std::shared_ptr<Cell> EvaluateIf(std::shared_ptr<Cell> cell,
                std::shared_ptr<Environment> currentEnv);
        static std::shared_ptr<Cell> EvaluateDefine(std::shared_ptr<Cell> cell,
                std::shared_ptr<Environment> currentEnv);
        static std::shared_ptr<Cell> EvaluateSet(std::shared_ptr<Cell> cell,
                std::shared_ptr<Environment> currentEnv);
        static std::shared_ptr<Cell> EvaluateLambda(std::shared_ptr<Cell> cell,
                std::shared_ptr<Environment> currentEnv);
        static std::shared_ptr<Cell> EvaluateFuncs(std::shared_ptr<Cell> cell,
                std::shared_ptr<Environment> currentEnv);
        static std::shared_ptr<Cell> EvaluateSetC(std::shared_ptr<Cell> cell,
                std::shared_ptr<Environment> currentEnv);


        std::shared_ptr<Environment> globalEnvironment;
};
