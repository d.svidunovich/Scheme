#include "Tokenizer.h"


Tokenizer::Tokenizer(std::istream* in):
    in_(in), type_(UNDEFINED), number_(0), symbol_("") {}


void Tokenizer::Read() {
    if (type_ != UNDEFINED) {
        return;
    }


    while (in_ -> peek() != EOF && isScipchar(in_ -> peek())) {
        in_ -> get();
    }

    symbol_ = "";
    while (42) {
        char c = in_ -> peek();
        if (c == EOF) {
            break;
        }

        if (!isSeparator(c)) {
            symbol_ += c;
            in_ -> get();
        } else {
            if (symbol_.empty()) {
                symbol_ += c;
                in_ -> get();
            }
            break;
        }
    }

    AnalizeType();
}


void Tokenizer::Consume() {
    type_ = UNDEFINED;
}


Tokenizer::TokenType Tokenizer::GetType() const {
    return type_;
}


std::string Tokenizer::GetSymbol() const {
    return symbol_;
}


int64_t Tokenizer::GetNumber() const {
    return number_;
}


void Tokenizer::AnalizeType() {
    if (symbol_.empty()) {
        type_ = END;
        return;
    }

    std::string::iterator start = symbol_.begin();
    if (start != symbol_.end() && start + 1 != symbol_.end() &&
            (*start == '-' || *start == '+')) {
        start++;
    }

    if (std::all_of(start, symbol_.end(),
                [](char c){ return '0' <= c && c <= '9'; })) {
        type_ = NUMBER;
        number_ = std::stoll(symbol_);
    } else {
        type_ = SYMBOL;
    }
}


bool Tokenizer::isSeparator(char c) {
    return find(separators_.begin(), separators_.end(), c) !=
        separators_.end();
}


bool Tokenizer::isScipchar(char c) {
    return find(skipchars_.begin(), skipchars_.end(), c) !=
        skipchars_.end();
}

