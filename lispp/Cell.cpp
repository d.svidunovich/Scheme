#include "Cell.h"


Cell::Cell(CellType type): type_(type) {}
Cell::Cell (const std::string& value): type_(SYMBOL), symbol_(value) {}
Cell::Cell(int64_t value): type_(NUMBER), number_(value) {} 
Cell::Cell(BuiltInProc bip): type_(BUILT_IN_PROC), bip_(bip) {} 


void Cell::Add(const std::shared_ptr<Cell>& cell) {
    list_.push_back(cell);
}


size_t Cell::Size() const {
    return list_.size();
}


std::vector<std::shared_ptr<Cell> >::iterator Cell::begin() {
    return list_.begin();
}


std::vector<std::shared_ptr<Cell> >::iterator Cell::end() {
    return list_.end();
}


std::vector<std::shared_ptr<Cell> >::const_iterator Cell::begin() const {
    return list_.begin();
}


std::vector<std::shared_ptr<Cell> >::const_iterator Cell::end() const {
    return list_.end();
}


void Cell::SetType(CellType type) {
    type_ = type; 
}


void Cell::SetEnvironment(std::shared_ptr<Environment> env) {
    currentEnv_ = env;
}


typename Cell::CellType Cell::GetType() const {
    return type_;
}


std::string Cell::GetSymbol() const {
    return symbol_;
}


int64_t Cell::GetNumber() const {
    return number_;
}


Cell::BuiltInProc Cell::GetBuildInProc() const {
    return bip_;
}


std::shared_ptr<Environment> Cell::GetEnvironment() const {
    return currentEnv_;
}


std::shared_ptr<Cell> Cell::PrepareCell(Tokenizer* tokenizer) {
    tokenizer -> Read();
    if (tokenizer -> GetType() == Tokenizer::END) {
        return std::shared_ptr<Cell> (nullptr);
    }

    if (tokenizer -> GetType() == Tokenizer::NUMBER) {
        tokenizer -> Consume();
        return std::shared_ptr<Cell> (new Cell(tokenizer -> GetNumber()));
    } else if (tokenizer -> GetType() == Tokenizer::SYMBOL) {
        if (tokenizer -> GetSymbol() == "(") {
            tokenizer -> Consume();
            std::shared_ptr<Cell> current(new Cell());

            while (true) {
                auto additional = Cell::PrepareCell(tokenizer);
                if (additional) {
                    if (additional -> GetType() == Cell::SYMBOL &&
                            additional -> GetSymbol() == ")") {
                        tokenizer -> Consume();
                        break;
                    }
                    current -> Add(additional); 
                } else {
                    throw SyntaxException();
                    //break;
                }
            }

            return current;
        } else if (tokenizer -> GetSymbol() == "'") {
            tokenizer -> Consume();
            std::shared_ptr<Cell> current(new Cell());
            current -> Add(std::make_shared<Cell> ("quote"));
            current -> Add(Cell::PrepareCell(tokenizer));
            return current;
        }
    }

    tokenizer -> Consume();
    return std::shared_ptr<Cell> (new Cell(tokenizer -> GetSymbol()));
}


std::string Cell::ToString() const {
    if (type_ == LIST) {
        std::string result = "(";
        for (auto it = begin(); it != end(); it++) {
            if (it -> get() -> GetType() == SYMBOL &&
                    it -> get() -> GetSymbol() == ".") {
                if ((++it) -> get() -> GetType() == LIST) {
                    std::string next = it -> get() -> ToString();
                    result.append(next.begin() + 1,
                            next.begin() + next.size() - 1);
                    result += " ";
                } else {
                    result += ". ";
                    result += (*it) -> ToString() + " ";
                }
            } else {
                result += (*it) -> ToString() + " ";
            }
        }

        while (result.back() == ' ') {
            result.pop_back();
        }
        result += ')';
        return result;
    } else if (type_ == LAMBDA) {
        return "<Lambda>";
    } else if (type_ == BUILT_IN_PROC) {
        return "<Proc>";
    } else if (type_ == NUMBER) {
        return std::to_string(number_);
    }

    return symbol_;
}
