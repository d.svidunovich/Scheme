#include "Exceptions.h"

const char* SyntaxException::what() const throw() {
    return "syntax error";
}


const char* NameException::what() const throw() {
    return "name error";
}


const char* RuntimeException::what() const throw() {
    return "runtime error";
}
