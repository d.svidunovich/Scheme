#include "SymbolBuiltInFunctions.h"


std::shared_ptr<Cell>
        isSymbolProc(const std::vector<std::shared_ptr<Cell> >& args) {
    if (args.size() != 1) {
        throw RuntimeException();
    }

    return std::make_shared<Cell> (
            args.front() -> GetType() == Cell::SYMBOL ? "#t" : "#f");
}
