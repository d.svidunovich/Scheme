#pragma once

#include "Cell.h"
#include "Exceptions.h"

#include <map>
#include <string>

class Cell;

class Environment {
    public:
        Environment(std::shared_ptr<Environment> outerEnv = nullptr);
        Environment(const Environment& other) = delete;
        Environment(Environment&& other) = default;
        ~Environment() = default;

        static std::shared_ptr<Environment> find(const std::string& variable,
                std::shared_ptr<Environment> currentEnv);

        void set(const std::string& variable, std::shared_ptr<Cell> value);

        Environment& operator = (const Environment& other) = delete;
        Environment& operator = (Environment&& other) = default;

        std::shared_ptr<Cell> operator[] (const std::string& variable);

    private:
        std::map<std::string, std::shared_ptr<Cell> > innerEnv_;
        std::shared_ptr<Environment> outerEnv_;
};
