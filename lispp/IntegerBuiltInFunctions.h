#pragma once

#include "Cell.h"
#include "Exceptions.h"

#include <memory>
#include <string>
#include <algorithm>
#include <vector>


std::shared_ptr<Cell>
        isNumberProc(const std::vector<std::shared_ptr<Cell> >& args);

std::shared_ptr<Cell>
        EqualProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        LessProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        GreaterProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        LessEqualProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        GreaterEqualProc(const std::vector<std::shared_ptr<Cell> >& args);


std::shared_ptr<Cell>
        PlusProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        MinusProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        MulProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        DivProc(const std::vector<std::shared_ptr<Cell> >& args);

std::shared_ptr<Cell>
        MinProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        MaxProc(const std::vector<std::shared_ptr<Cell> >& args);

std::shared_ptr<Cell>
        AbsProc(const std::vector<std::shared_ptr<Cell> >& args);
