#pragma once

#include "Cell.h"
#include "Exceptions.h"
#include "Interpreter.h"

#include <memory>
#include <string>
#include <algorithm>
#include <vector>


std::shared_ptr<Cell>
        isBooleanProc(const std::vector<std::shared_ptr<Cell> >& args);


std::shared_ptr<Cell>
        NotProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        AndProc(const std::vector<std::shared_ptr<Cell> >& args);
std::shared_ptr<Cell>
        OrProc(const std::vector<std::shared_ptr<Cell> >& args);
