#pragma once

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

class Tokenizer {
    public:
        enum TokenType {
            UNDEFINED,
            SYMBOL,
            NUMBER,
            END
        };

        Tokenizer(std::istream* in);
        Tokenizer(const Tokenizer& other) = delete;
        Tokenizer(Tokenizer&& other) = default;
        ~Tokenizer() = default;


        void Read();
        void Consume();

        TokenType GetType() const;
        std::string GetSymbol() const;
        int64_t GetNumber() const;


        Tokenizer& operator = (const Tokenizer& other) = delete;
        Tokenizer& operator = (Tokenizer&& other) = default;

    private:
        void AnalizeType();
        bool isSeparator(char c);
        bool isScipchar(char c);

        std::vector<char> skipchars_ = {' ', '\n'};
        std::vector<char> separators_ = {' ', '\n', '(', ')', '\''};

        std::istream* in_;
        TokenType type_;
        int64_t number_;
        std::string symbol_;
};
